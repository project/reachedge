# LOCALiQ Drupal Module

A Drupal module to install the [LOCALiQ](https://localiq.com) tracking code.

## Features

* Enables the [LOCALiQ](https://localiq.com) tracking functionality on Drupal sites.
* Automatically avoids adding the tracking code to `/admin` and `/user`.
* Compatible with Drupal 8.8.0 or above.
* Tested up to Drupal 9.4.1.

## Installation

1. Find and click the link for a compatible version on [Releases for LOCALiQ](https://www.drupal.org/project/reachedge/releases).
2. Right click and copy the link for *Download tar.gz*.
3. In the Drupal dashboard, navigate to the *Reports* page, then click *Available updates*.
4. Click on the *Add new module or theme* button.
5. Paste the tar.gz link into the *Add from a URL* box, then click *Continue*.
6. When the installation completes successfully, click on *Enable newly added modules*.
7. Locate *LOCALiQ* under the Lead Conversion section, check the box next to it, then click *Install* at the bottom of the page.

## Entering your tracking code ID

1. In the Drupal dashboard, navigate to the *Configuration* menu.
2. Click the *LOCALiQ* link.
3. Enter your tracking code ID into the ID field, then click the *Save configuration* button.
